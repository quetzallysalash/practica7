package mx.ticketit.practica7

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import mx.ticketit.practica7.model.Contact
import mx.ticketit.practica7.widget.ContactsAdapter

class MainActivity : AppCompatActivity() {

    /*
    Nephtali Gómez Arcive
    Part 5 - Creación de lista tipo contact con valores
     */

    private var Contacts = listOf(
        Contact("Nephtali", 26, "nga@ticketit.mx"),
        Contact ("Robot", 26, "robot@ticketit.mx")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        /*
   Nephtali Gómez Arcive
   Part 5 - Android extensions - implementación de list_recycler_view
    */

        list_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ContactsAdapter(Contacts)
        }

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
