package mx.ticketit.practica7.widget

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mx.ticketit.practica7.model.Contact

// Nephtali Gómez Arcive
// Parte 4 - Creación de clase ContactsAdapter

// Parte 4 - Parámetro de lista de tipo Contact

class ContactsAdapter(private var list : List<Contact>) : RecyclerView.Adapter<ContactViewHolder>() {

    // Creación de métodos
    // Se ajusta en parte 5 errores en sobreescritura y adapter (commit)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ContactViewHolder {
         val inflater = LayoutInflater.from(parent.context)
         return ContactViewHolder(inflater,parent)
    }

     override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
         val contact : Contact = list[position]
         holder.bind(contact)
    }


     override fun getItemCount():Int {
        return list.size
    }

}
